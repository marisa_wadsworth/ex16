﻿using System;

namespace ex16
{
    class Program
    {
        static void Main(string[] args)
        {
             //Start the program with Clear();
            Console.Clear();

            //Delcreaing Variables
            //guestName : string

            string guestName;
            
            // Using "* * *" to decorate my program
            Console.WriteLine("* * * * * * * * * * * * * * * * * * *");
            Console.WriteLine("* * * * * * * * * * * * * * * * * * *");
            Console.WriteLine("* * * * Hello! Welcome to my app * * *");
            Console.WriteLine("* * * * * * * * * * * * * * * * * * *");
            Console.WriteLine("* * * * * * * * * * * * * * * * * * *");

            //asking the user what their name is
            Console.WriteLine("What is your name?");
            guestName = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine($"Your name is: {guestName} "); 
            Console.WriteLine();
    
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
